// Endpoint comunication
// https://cloud.google.com/appengine/docs/java/endpoints/consume_js

function authorize(immediate) {
	gapi.auth.authorize({
		client_id: '862163872842-f8adttr8j8um140c3paroj8e341u24mq.apps.googleusercontent.com',
		scope: 'https://www.googleapis.com/auth/userinfo.email',
		immediate: immediate // lze si vyskakovat prihlasovacim oknem?
	}, function(authResult){
		console.log(authResult);
		if (authResult && !authResult.error) {
			console.log("Authorized, trying get userinfo...");
			var request = gapi.client.oauth2.userinfo.get().execute(function(resp) {
				console.log(resp);
				if (!resp.code) {
					console.log("Authorized succesfly");
					onAuthorized(resp);
				} else {
					authorize(false);
				}
			});
			
		} else {
			console.log("Handle unauthorized");
		}
	});
}

function signOut() {
	gapi.auth.signOut();
	document.getElementById("content").style.display = "none";
	document.getElementById("signinButton").style.display = "block";
	console.log("odhlasen");
}

function register(info){
	console.log("registration...");
	gapi.client.messenger.profiles.register({
		'name': info.name,
		'picture': info.picture,
		'regId': ''
	}).execute(function(resp){
		console.log("registration ok");
		console.log(resp);
		loggedUser = resp;
	});
}

function contactsList(){
	document.getElementById("contacts").innerHTML = "";
	gapi.client.messenger.contacts.list().execute(function(resp) {
		console.log("loading contacts...");
		for(key in resp.items){
			document.getElementById("contacts").innerHTML +=
				'<tr>' +
				'<td onclick="changeChat(this)">' +
				'<div class="name">' + resp.items[key].name + '</div>'+
				'<div class="lastMsg" id="lastMess' + resp.items[key].userId + '"></div>'+
				'<div class="userId">' + resp.items[key].userId + '</div>'+
				'</td>'+
				'</tr>';
			gapi.client.messenger.messages.by({
				'from': resp.items[key].userId
			}).execute(function(resp2) {
				console.log(resp2);
				document.getElementById("lastMess" + resp2.items[resp2.items.length-1].from).innerHTML =
					resp2.items[resp2.items.length-1].msg;
			});
		}
	});
}

function contactsAddOrRemove(name,userId){
	gapi.client.messenger.contacts.list().execute(function(resp) {
		var exist = false;
		console.log(resp.items);
		for(key in resp.items){
			if(resp.items[key].userId == userId) exist = true;
		}
		console.log("exist = "+exist);
		if(exist){
			contactsRemove(userId);
		}else{
			contactsAdd(name,userId);
		}
	});
}

function contactsAdd(name,userId){
	console.log("adding "+userId);
	gapi.client.messenger.contacts.add({
		'name': name,
		'userId': userId
	}).execute(function(resp){
		console.log(resp);
		contactsList(); // refresh
	});
}

function contactsRemove(userId){
	console.log("removing "+userId);
	gapi.client.messenger.contacts.remove({
		'userId': userId
	}).execute(function(resp){
		console.log(resp);
		contactsList(); // refresh
	});
}

function messagesBy(userId){
	document.getElementById("chatArea").innerHTML = "";
	gapi.client.messenger.messages.by({
		'from': userId
	}).execute(function(resp) {
		console.log("loading messages...");
		console.log(resp);
		lat = 0.0;
		long = 0.0;
		for(key in resp.items){
			var picture = resp.items[key].from in profilesCache ? profilesCache[resp.items[key].from].picture : "empty_profile.gif";
			var name = resp.items[key].from in profilesCache ? profilesCache[resp.items[key].from].name : "Stranger";
			document.getElementById("chatArea").innerHTML +=
				"<div class='chat_wrapper' title='Sent at " + resp.items[key].date + "'>" +
				"<div class = 'left_chat'><img class='profile_img' src='" + picture + "'></div>" +
				"<div class = 'right_chat'>" +
				"<span class='from'>" + name + "</span>" +
				"<br/>" +
				"<span class='msg'>" + resp.items[key].msg +
				(
					resp.items[key].attachment ?
					"<br/><img src=\"data:image/png;base64," + resp.items[key].attachment + "\" />" : ""
				) +
				"</span>" +
				"</div>" +
				"</div>";
			if(resp.items[key].from == userId){
				console.log(resp.items[key]);
				lat = resp.items[key].latitude;
				long = resp.items[key].longitude;
			}			
		}
		console.log(lat);
		console.log(long);
		window.map.setCenter(new google.maps.LatLng(lat, long));
		
		window.marker.setPosition(new google.maps.LatLng(lat, long));
		google.maps.event.trigger(window.map, "resize");
		$("#chatArea").scrollTop($("#chatArea")[0].scrollHeight);
		
	});
}

function profilesGet(userId){
	gapi.client.messenger.profiles.get({
		'userId': userId
	}).execute(function(resp) {
		if(resp){
			console.log("getted profile: "+userId);
			console.log(resp);
			profilesCache[userId] = resp;
		}else{
			console.log("getting profile: "+userId+" returned false");
		}
	});
}

function profilesList(){
	$("#contacts").find(".selected").removeClass("selected");
	document.getElementById("chatArea").innerHTML = "";
	gapi.client.messenger.profiles.list().execute(function(resp) {
		console.log("loading user list...");
		console.log(resp);
		for(key in resp.items){
			document.getElementById("chatArea").innerHTML +=
				"<div class='userlist_wrapper' onclick=\"contactsAddOrRemove(this.getElementsByClassName('right_chat')[0].getElementsByClassName('name')[0].innerHTML, this.getElementsByClassName('right_chat')[0].getElementsByClassName('userId')[0].innerHTML)\">" +
				"<div class = 'left_chat'><img class='profile_img' src='"+resp.items[key].picture+"'></div>" +
				"<div class = 'right_chat'>" +
				"<span class=\"name\">" + resp.items[key].name + "</span>" +
				"<br/>" +
				"<span class=\"email\">" + resp.items[key].email + "</span>" +
				"<br/>" +
				"<span class=\"userId\">" + resp.items[key].userId + "</span>" +
				"</div>" +
				"</div>";
			console.log(resp.items[key]);
		}
	});
}

function messagesSendTo(to,msg,latitude,longitude){
	gapi.client.messenger.messages.sendTo({
		'to': to,
		'msg': msg,
		'latitude': latitude,
		'longitude': longitude
	}).execute(function(resp){
		console.log(resp);
		messagesBy(currentChatUserId);
	});
}

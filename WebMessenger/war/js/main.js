// Skeleton of application

var apisToLoad;
var currentChatUserId = null;
var loggedUser;
var profilesCache = [];
var gps = null;
var long = 0.0;
var lat = 0.0;
var map;
var marker;


function initialize() {
    var mapCanvas = document.getElementById('map-canvas');
    var mapOptions = {
      center: new google.maps.LatLng(44.5403, -78.5463),
      zoom: 8,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    window.map = new google.maps.Map(mapCanvas, mapOptions);
    var myLatlng = new google.maps.LatLng(lat, long)

	window.marker = new google.maps.Marker({
	    position: myLatlng,
	    map: window.map,
	    title: 'I\'m your friend :)'
	});
    map.setZoom(16);
}


function init() {
	console.log("init()");
	/*self.setInterval(function(){
		if (window.currentChatUserId)
			messagesBy(window.currentChatUserId);
		
	},1*5*1000);*/

	document.getElementById("content").style.display = "none";
	apisToLoad = 2;
	gapi.client.load("messenger", "v1", onLoad, "https://" + window.location.host + "/_ah/api");
	gapi.client.load("oauth2", "v2", onLoad);
	getLocation();
	
	var map;
}

function onLoad() {
	if (--apisToLoad == 0) {
		console.log("Trying authorize...");
		authorize(true); // pasivni - nevyskakovat (lze jen kdyz bylo kliknuto na tlacitko)
	}
}

function onAuthorized(info) {
	console.log("Signed "+info.name+", registering...");
	console.log(info);
	register(info);
	
	console.log("registered, contacts...");
	contactsList();
	
	console.log("showing...");
	document.getElementById("content").style.display = "block";
	document.getElementById("signinButton").style.display = "none";
}

function changeChat(td) {
	currentChatUserId = $(td).find(".userId").text();
	
	console.log("get profile "+currentChatUserId+" and "+loggedUser.userId);
	
	profilesGet(currentChatUserId);
	profilesGet(loggedUser.userId);
	
	messagesBy(currentChatUserId);
	document.getElementById("writeArea").style.display = "block";
	document.getElementById("chatArea").style.height = "300px";
	$("#contacts").find(".selected").removeClass("selected");
	$(td).parent().addClass("selected");
}

function send(){
	if(document.getElementById("newMessageString").value){
		console.log("sending "+document.getElementById("newMessageString").value+" to "+currentChatUserId);
		messagesSendTo(currentChatUserId,document.getElementById("newMessageString").value, gps ? gps.latitude : 0, gps ? gps.longitude : 0);
		document.getElementById("newMessageString").value = "";
	}
}

function userList(){
	currentChatUserId = null;
	profilesList();
	document.getElementById("writeArea").style.display = "none";
	document.getElementById("chatArea").style.height = "415px";
}

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.watchPosition(function(position){
        	console.log("Loaded GPS:");
        	console.log(position);
        	gps = position.coords;
        });
    }
}

function change_map(){
	var elem = document.getElementById("map_map");
    if (elem.innerHTML=="Show map") elem.innerHTML = "Hide map";
    else elem.innerHTML = "Show map";
}

function handleNoGeolocation(errorFlag) {
	  if (errorFlag) {
	    var content = 'Error: The Geolocation service failed.';
	  } else {
	    var content = 'Error: Your browser doesn\'t support geolocation.';
	  }

	  var options = {
	    map: map,
	    position: new google.maps.LatLng(60, 105),
	    content: content
	  };

	  var infowindow = new google.maps.InfoWindow(options);
	  map.setCenter(options.position);
	}

google.maps.event.addDomListener(window, 'load', initialize);

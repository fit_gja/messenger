package gja.messenger;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;

@Entity
public class Contact implements Comparable<Contact> {
	
	@Id
	private Long id;
	
	@Index // musi byt nad vsemi, pres ktere se chci dotazovat
	private String name;
	
	@Index
	private String userId;
	
	@Index
	private String owner;
	
	@Ignore
    private String email;
	
    @Ignore
    private String picture;
	
	public Contact(){}
	
	public Contact(String name, String userId, String owner){
		this.name = name;
		this.userId = userId;
		this.owner = owner;
	}
	
	public Long getId() {
		return id;
	}
	
	public String getName(){
		return name;
	}
	
	public String getEmail(){
		return email;
	}
	
	public String getUserId(){
		return userId;
	}
	
	public String getOwner(){
		return owner;
	}
	
	public String getPicture(){
		return picture;
	}
	
	public void setPicture(String picture){
		this.picture = picture;
	}
	
	public int compareTo(Contact other) {
		return userId.compareTo(other.userId);
	}
}

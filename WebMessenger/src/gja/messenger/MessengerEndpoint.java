package gja.messenger;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.Named;
import com.google.appengine.api.users.User;
import com.googlecode.objectify.cmd.Query;
import com.googlecode.objectify.cmd.QueryKeys;

@Api(name = "messenger",
     scopes = {"https://www.googleapis.com/auth/userinfo.email"},
     clientIds = {"862163872842-f8adttr8j8um140c3paroj8e341u24mq.apps.googleusercontent.com",  // web client id
	              "862163872842-mh0qov63n39ktnsr2m1ko5md60ma2j51.apps.googleusercontent.com"}, // android client id
     audiences = {"862163872842-f8adttr8j8um140c3paroj8e341u24mq.apps.googleusercontent.com"}  // web client id
)
public class MessengerEndpoint {

	private static final Logger log = Logger.getLogger(MessengerEndpoint.class.getName());
	
	@ApiMethod(name = "profiles.register")
	public Profile profilesRegister(Profile info, User user) {
		if(user == null) throw new SecurityException("Not logged");
		if(user.getUserId() == null) throw new SecurityException("User id is null");
		
		Query<Profile> query = OfyService.ofy().load().type(Profile.class).filter("userId", user.getUserId());
		Profile profile;
		if(query.count() <= 0){
			profile = new Profile(user.getEmail(),
			                      (info.getName() == null || info.getName().length() <= 0) ? user.getEmail() : info.getName(),
			                      info.getPicture() == null ? "" : info.getPicture(),
			                      user.getUserId(),
			                      info.getRegId() == null ? "" : info.getRegId());
		}else{
			profile = query.first().get();
			profile.setEmail(user.getEmail());
			if(info.getName() != null && info.getName().length() > 0) profile.setName(info.getName());
			if(info.getPicture() != null && info.getPicture().length() > 0) profile.setPicture(info.getPicture());
			if(info.getRegId() != null && info.getRegId().length() > 0) profile.setRegId(info.getRegId());
		}
		OfyService.ofy().save().entity(profile).now();
		log.info("Zaregistrovan: " + (user==null ? "NULL" : profile.getEmail() + ", " + profile.getRegId()));
		return profile;
	}
	
	@ApiMethod(name = "profiles.list")
	public List<Profile> profilesList(User user) {
		return OfyService.ofy().load().type(Profile.class).list();
	}
	
	@ApiMethod(name = "profiles.get")
	public Profile profilesGet(@Named("userId") String userId, User user) {
		Query<Profile> query = OfyService.ofy().load().type(Profile.class).filter("userId", userId);
		if(query.count() <= 0){
			log.warning("Profile "+userId+" was not found");
			return null;
		}else{
			Profile profile = query.first().get(); 
			log.warning("Profile "+userId+" getted successly: "+profile.getEmail());
			return profile;
		}
	}
	
	@ApiMethod(name = "contacts.list")
	public List<Contact> contactsList(User user) {
		if(user == null) throw new SecurityException("Not logged");
		return OfyService.ofy().load().type(Contact.class).filter("owner", user.getUserId()).list();
	}
	
	@ApiMethod(name = "contacts.add")
	public void contactsAdd(@Named("name") String name,
		                    @Named("userId") String userId,
		                    User user) {
		if(user == null) throw new SecurityException("Not logged");
		Contact contact = new Contact(name, userId, user.getUserId());
	    OfyService.ofy().save().entity(contact).now();
	}
	
	@ApiMethod(name = "contacts.remove")
	public void contactsRemove(@Named("userId") String userId, User user) {
		if(user == null) throw new SecurityException("Not logged");
		QueryKeys<Contact> keys = OfyService.ofy().load().type(Contact.class).filter("userId", userId).filter("owner", user.getUserId()).keys();
		OfyService.ofy().delete().keys(keys);
	}
	
	@ApiMethod(name = "messages.by")
	public List<ChatMessage> messagesBy(@Named("from") String from, User user) {
		if(user == null) throw new SecurityException("Not logged");
		List<ChatMessage> list = OfyService.ofy().load().type(ChatMessage.class).filter("from", from).filter("to", user.getUserId()).list();
		log.warning("list1.size = " + list.size());
		List<ChatMessage> list2 = OfyService.ofy().load().type(ChatMessage.class).filter("from", user.getUserId()).filter("to", from).list();
		log.warning("list2.size = " + list2.size());
		list.addAll(list2);
		Collections.sort(list, Collections.reverseOrder());
		log.warning("list12.size = " + list.size());
		return list;
	}
	
	@ApiMethod(name = "messages.sendTo")
	public void messagesSendTo(ChatMessage message, User user) {
		
		if(user == null) throw new SecurityException("Not logged");
		log.warning("from "+user.getEmail()+", to "+message.getTo()+": "+message.getMsg());
		
		// ulozeni do databaze
		message.setFrom(user.getUserId());
		message.setDate(new Date());
		OfyService.ofy().save().entity(message).now();
		String to = message.getTo();
		
		Query<Profile> query = OfyService.ofy().load().type(Profile.class).filter("userId", to);
		if(query.count() <= 0){
			log.warning("Profile userId="+to+" was not found");
			return;
		}
		Profile profile = query.first().get();
		
		if(OfyService.ofy().load().type(Contact.class).filter("userId", user.getUserId()).filter("owner", to).count() <= 0){
			log.warning("Adding contact...");
			Profile from = OfyService.ofy().load().type(Profile.class).filter("userId", user.getUserId()).first().get();
			Contact contact = new Contact(from.getName(), user.getUserId(), to);
		    OfyService.ofy().save().entity(contact).now();
		    log.warning("Added contact");
		}else{
			log.warning("Contact alredy exist");
		}
		
		// rozeslani na androidi zarizeni
		Sender sender = new Sender("AIzaSyCZisLvwevSRyVJHo_hUV9JIgPr-pA7IUQ");
		Message androidMessage = new Message.Builder()
		    .delayWhileIdle(false)
		    .addData("fromId", message.getFrom())
		    .addData("toId", message.getTo())
		    .addData("msg", message.getMsg())
		    .addData("date", Long.toString(message.getDate().getTime()))
		    .addData("latitude", Double.toString(message.getLatitude()))
		    .addData("longitude", Double.toString(message.getLongitude()))
		    .build();
		try {
			Result result = sender.send(androidMessage, profile.getRegId(), 5);
			//List<String> userIds = new ArrayList<String>();
            //userIds.add(userId);
            //MulticastResult result = sender.send(message, userIds, 5);
			log.warning("from "+user.getEmail()+", to "+profile.getRegId()+": "+message.getMsg()+", result="+result.getErrorCodeName());
        } catch (IOException e) {
            log.warning(e.getMessage());
        }
	}
	
}

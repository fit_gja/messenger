package gja.messenger;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class Profile {
	
	@Id
	private Long id;
	
	@Index
    private String userId;
    
    @Index
    private String email;
    
    private String name;
    
    private String picture;
    
    @Index
    private String regId;
    
    public Profile() {}
    
    public Profile(String email, String name, String picture, String userId, String regId) {
    	this.userId = userId;
    	this.email = email;
        this.name = name;
        this.picture = picture;
        this.regId = regId;
    }
    
    public Long getId() {
		return id;
	}
    
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
    
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getPicture() {
		return picture;
	}
	
	public void setPicture(String picture) {
		this.picture = picture;
	}
	
	public String getRegId() {
		return regId;
	}
	
	public void setRegId(String regId) {
		this.regId = regId;
	}
}

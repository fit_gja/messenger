package gja.messenger;

import java.util.Date;

import com.google.appengine.api.datastore.Blob;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class ChatMessage implements Comparable<ChatMessage> {
	
	@Id
	private Long id;
	
	@Index
	private String from;
	
	@Index
	private String to;
	
	private String msg;
	
	private Blob attachment;
	
	@Index
	private Date date;
	
	private double latitude;
	private double longitude;
	
	public ChatMessage(){} // must have a no-arg constructor
	
	public ChatMessage(String from, String to, String msg, Blob attachment, Date date, double lat, double longt) {
		super();
		this.from = from;
		this.to = to;
		this.msg = msg;
		this.attachment = attachment;
		this.date = date;
		this.latitude = lat;
		this.longitude = longt;
	}
	
	public Long getId() {
		return id;
	}
	
	public String getFrom() {
		return from;
	}
	
	public void setFrom(String from) {
		this.from = from;
	}
	
	public String getTo() {
		return to;
	}
	
	public String getMsg() {
		return msg;
	}
	
	public Blob getAttachment() {
		return attachment;
	}
	
	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}
	
	public double getLatitude() {
		return latitude;
	}
	
	public double getLongitude() {
		return longitude;
	}
	
	@Override
	public int compareTo(ChatMessage o) {
		return o.getDate().compareTo(getDate());
	}
}

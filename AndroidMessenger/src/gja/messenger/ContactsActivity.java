package gja.messenger;

import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.appspot.gja_messenger.messenger.model.Contact;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.plus.Plus;

public class ContactsActivity extends ListActivity implements
		ConnectionCallbacks, OnConnectionFailedListener  {

	private SharedPreferences settings;
	private GoogleApiClient mGoogleApiClient;
	private boolean mIntentInProgress;
	private boolean mSignInClicked;
	private static final int RC_SIGN_IN = 0;
	private ArrayList<Contact> contacts = new ArrayList<Contact>();
	public GcmUtil gcmUtil;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().getDecorView().setBackgroundColor(Color.WHITE);
		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.addConnectionCallbacks((ConnectionCallbacks) this)
				.addOnConnectionFailedListener(this).addApi(Plus.API)
				.addScope(Plus.SCOPE_PLUS_LOGIN).build();

		settings = getSharedPreferences("GjaMessenger", 0);
		gcmUtil = new GcmUtil(getApplicationContext()); // zaregistruje push
		Endpoint endpoint = new Endpoint(MainActivity.credential);

		endpoint.contactsList(this);
		ContactAdapter adapter = new ContactAdapter(this,
				R.layout.main_list_item, contacts);					
		setListAdapter(adapter);
	
		
		
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayShowTitleEnabled(false);

		getListView().setOnTouchListener(new OnSwipeTouchListener(ContactsActivity.this) {
			
			@Override
			public void onSwipeRight(int id) {
				super.onSwipeLeft();
				Toast.makeText(ContactsActivity.this, "Swipe right!" + id, Toast.LENGTH_SHORT).show();
				
				if(id != -1) {
					Contact contact = contacts.get(id);
					Endpoint.instance.contactsRemove(contact.getUserId(), id, ContactsActivity.this);
				}
			}
			
		});
	}

	
	
	protected void onStart() {
		super.onStart();
		mGoogleApiClient.connect();
	}

	protected void onStop() {
		super.onStop();

		if (mGoogleApiClient.isConnected()) {
			mGoogleApiClient.disconnect();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.contacts, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();
		if (itemId == R.id.action_add) {
			Intent intent = new Intent(this, AddContactActivity.class);
			startActivity(intent);
			return true;
		} else if (itemId == R.id.action_logout
				&& !mGoogleApiClient.isConnecting()) {
			if (mGoogleApiClient.isConnected()) {
				Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
				mGoogleApiClient.disconnect();
				mGoogleApiClient.connect();
			}
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		Intent intent = new Intent(this, ChatActivity.class);
		intent.putExtra(Common.PROFILE_ID, contacts.get(position).getUserId());
		intent.putExtra(Common.PROFILE_NAME, contacts.get(position).getName());
		intent.putExtra(Common.PROFILE_OWNER, contacts.get(position).getOwner());
		intent.putExtra(Common.PROFILE_PICTURE, contacts.get(position).getPicture());
		startActivity(intent);
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		if (!mIntentInProgress) {
			if (mSignInClicked && result.hasResolution()) {
				// The user has already clicked 'sign-in' so we attempt to
				// resolve all
				// errors until the user is signed in, or they cancel.
				try {
					result.startResolutionForResult(this, RC_SIGN_IN);
					mIntentInProgress = true;
				} catch (SendIntentException e) {
					// The intent was canceled before it was sent. Return to the
					// default
					// state and attempt to connect to get an updated
					// ConnectionResult.
					mIntentInProgress = false;
					mGoogleApiClient.connect();
				}
			}
		}
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		mSignInClicked = false;
		Common.setEmail(Plus.AccountApi.getAccountName(mGoogleApiClient));
		Toast.makeText(this, "User is connected!", Toast.LENGTH_LONG).show();
	}

	@Override
	public void onConnectionSuspended(int cause) {
		mGoogleApiClient.connect();
	}

	@Override
	protected void onActivityResult(int requestCode, int responseCode,
			Intent intent) {
		if (requestCode == RC_SIGN_IN) {
			if (responseCode != RESULT_OK) {
				mSignInClicked = false;
			}

			mIntentInProgress = false;

			if (!mGoogleApiClient.isConnected()) {
				mGoogleApiClient.reconnect();
			}
		}
	}

	public void setContactList(List<Contact> list) {
		contacts.clear();
		contacts.addAll(list);
		((ArrayAdapter<Contact>) getListAdapter()).notifyDataSetChanged();
	}
	
	public void removeContact(int id) {
		Contact c = contacts.get(id);
		contacts.remove(c);		
		 
		((ArrayAdapter<Contact>) getListAdapter()).notifyDataSetChanged();
		
	}



	public static class ContactAdapter extends ArrayAdapter<Contact> {
		
	    private static class ViewHolder {
	        private TextView itemView;
	    }

	    public ContactAdapter(Context context, int textViewResourceId, ArrayList<Contact> items) {
	        super(context, textViewResourceId, items);
	    }

	    public View getView(int position, View convertView, ViewGroup parent) {
	    	View v = convertView;
    	    if(v==null) {
    	    	LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	        v=vi.inflate(R.layout.main_list_item, parent, false);
    	        //v.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 80));
    	        v.requestLayout();
    	    }
	  
    	    Contact message = getItem(position);  	    
   			TextView tv = (TextView)v.findViewById(R.id.text1);
   			tv.setText(message.getName());
   			tv = (TextView)v.findViewById(R.id.text2);
   			tv.setText(message.getEmail());
   			Endpoint.instance.loadProfilePicture(message.getPicture(), (ImageView)v.findViewById(R.id.avatar));
   			return v;
				
			
	    }
	}
}

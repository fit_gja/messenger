package gja.messenger;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.Toast;

import com.appspot.gja_messenger.messenger.Messenger;
import com.appspot.gja_messenger.messenger.model.ChatMessage;
import com.appspot.gja_messenger.messenger.model.Contact;
import com.appspot.gja_messenger.messenger.model.Profile;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.json.jackson2.JacksonFactory;

/*
 * Tato trida slouzi ke komunikaci se serverem.
 * Tonda: metody onPostExecute() uprav, aby hazely vysledky kam potrebujes
 * MainActivity v parametru muze byt nahrazen cimkoli co bude potreba v onPostExecute() 
 */
public class Endpoint {
	
	public static Endpoint instance = null;
	
	private Messenger messenger;
	private Logger log = Logger.getLogger("ENDPOINT");
	
	public Endpoint(GoogleAccountCredential credential) {
		Messenger.Builder builder =	new Messenger.Builder(
				AndroidHttp.newCompatibleTransport(),
				new JacksonFactory(),
				credential);
		messenger = builder.build();
		instance = this;
	}
	
	public void register(String name, String picture, String regId, final Context context) {
		log.log(Level.INFO, "register");
		final Profile profile = new Profile();
		profile.setName(name);
		profile.setPicture(picture);
		profile.setRegId(regId);
		
		new AsyncTask<Void, Void, Exception>() {
			protected Exception doInBackground(Void... trash){
				try {
					messenger.profiles().register(profile).execute();
				} catch (IOException e) {
					return e;
				}
				return null;
			}
			protected void onPostExecute(Exception e){
				log.log(Level.INFO, "onPostExecute register");
				if(e == null){
					Toast.makeText(context, "Registered :)", Toast.LENGTH_SHORT).show();
					log.log(Level.INFO, "registered :)");
				}else{
					Toast.makeText(context, "Registration failed :(", Toast.LENGTH_SHORT).show();
					log.log(Level.INFO, "registration failed :(");
				}
			}
		}.execute();
	}
	
	public void profilesList(final ListActivity activity) {
		Logger.getLogger("ENDPOINT").log(Level.INFO, "profilesList");
		
		new AsyncTask<Void, Void, List<Profile>>() {
			protected List<Profile> doInBackground(Void... trash){
				try {
					return messenger.profiles().list().execute().getItems();
				} catch (IOException e) {
					e.printStackTrace();
					return null;
				}
			}
			protected void onPostExecute(List<Profile> list){
				log.log(Level.INFO, "onPostExecute profilesList");
				if(list == null){
					Toast.makeText(activity, "Listing profiles failed :(", Toast.LENGTH_SHORT).show();
					log.log(Level.INFO, "Listing profiles failed :(");
				}else{
					Toast.makeText(activity, "Listed profiles: "+list.size(), Toast.LENGTH_SHORT).show();
					log.log(Level.INFO, "Listed profiles: "+list.size());
					
					if(activity instanceof AddContactActivity) {
						((AddContactActivity)activity).setProfileList(list);
					}
				}
			}
		}.execute();
	}
	
	public void getProfile(final String userId, final MainActivity activity) {
		Logger.getLogger("ENDPOINT").log(Level.INFO, "getProfile");
		
		new AsyncTask<Void, Void, Profile>() {
			protected Profile doInBackground(Void... trash){
				try {
					return messenger.profiles().get(userId).execute();
				} catch (IOException e) {
					e.printStackTrace();
					return null;
				}
			}
			protected void onPostExecute(Profile profile){
				log.log(Level.INFO, "onPostExecute profilesList");
				if(profile == null){
					Toast.makeText(activity, "Getting profile failed :(", Toast.LENGTH_SHORT).show();
					log.log(Level.INFO, "Getting profile failed :(");
				}else{
					Toast.makeText(activity, "Listed profile: "+profile.getName(), Toast.LENGTH_SHORT).show();
					log.log(Level.INFO, "Listed profile: "+profile.getName());
					// TODO: tady se to musi nasazet do GUI
				}
			}
		}.execute();
	}
	
	public void showMessageNotification(final String text, final String fromId, final String toId, final GcmBroadcastReceiver receiver) {
		Logger.getLogger("ENDPOINT").log(Level.INFO, "showMessageNotification");
		
		new AsyncTask<Void, Void, Profile>() {
			protected Profile doInBackground(Void... trash){
				try {
					return messenger.profiles().get(fromId).execute();
				} catch (IOException e) {
					e.printStackTrace();
					return null;
				}
			}
			protected void onPostExecute(Profile profile){
				log.log(Level.INFO, "onPostExecute showMessageNotification");
				if(profile == null){
					log.log(Level.WARNING, "Getting profile for notification failed");
					receiver.sendNotification(text, fromId, fromId, null, toId, true);
				}else{
					log.log(Level.INFO, "Getting profile for notification OK");
					receiver.sendNotification(text, fromId, profile.getName(), profile.getPicture(), toId, true);
				}
			}
		}.execute();
	}
	
	public void contactsList(final ContactsActivity activity) {
		Logger.getLogger("ENDPOINT").log(Level.INFO, "contactsList");
		
		new AsyncTask<Void, Void, List<Contact>>() {
			protected List<Contact> doInBackground(Void... trash){
				try {
					List<Contact> contacts = messenger.contacts().list().execute().getItems();
					if(contacts == null) return null;
					for(Contact c : contacts) {
						Profile profile = messenger.profiles().get(c.getUserId()).execute();
						c.setPicture(profile.getPicture());
						c.setEmail(profile.getEmail());
					}
					return contacts;
				} catch (IOException e) {
					e.printStackTrace();
					return null;
				}
			}
			protected void onPostExecute(final List<Contact> list){
				log.log(Level.INFO, "onPostExecute contactsList");
				if(list == null){
					Toast.makeText(activity, "Listing contacts failed :(", Toast.LENGTH_SHORT).show();
					log.log(Level.INFO, "Listing profiles failed :(");
				}else{
					Toast.makeText(activity, "Listed contacts: "+list.size(), Toast.LENGTH_SHORT).show();
					log.log(Level.INFO, "Listed contacts: "+list.size());
					
					activity.setContactList(list);
				}
			}
		}.execute();
	}
	
	public void contactsAdd(final String name, final String userId, final MainActivity activity) {
		Logger.getLogger("ENDPOINT").log(Level.INFO, "contactsAdd");
		
		new AsyncTask<Void, Void, Exception>() {
			protected Exception doInBackground(Void... trash){
				try {
					messenger.contacts().add(name, userId).execute();
				} catch (IOException e) {
					return e;
				}
				return null;
			}
			protected void onPostExecute(Exception e){
				log.log(Level.INFO, "onPostExecute contactsAdd");
				if(e == null){
					Toast.makeText(activity, "Contact added :)", Toast.LENGTH_SHORT).show();
					log.log(Level.INFO, "Contact added :)");
				}else{
					Toast.makeText(activity, "Contact adding failed :(", Toast.LENGTH_SHORT).show();
					log.log(Level.INFO, "Contact adding failed :(");
				}
			}
		}.execute();
	}
	
	public void contactsRemove(final String userId, final int listId, final ContactsActivity activity) {
		Logger.getLogger("ENDPOINT").log(Level.INFO, "contactsRemove");
		
		new AsyncTask<Void, Void, Exception>() {
			protected Exception doInBackground(Void... trash){
				try {
					messenger.contacts().remove(userId).execute();
				} catch (IOException e) {
					return e;
				}
				return null;
			}
			protected void onPostExecute(Exception e){
				log.log(Level.INFO, "onPostExecute contactsRemove");
				if(e == null){
					Toast.makeText(activity, "Contact removed :)", Toast.LENGTH_SHORT).show();
					log.log(Level.INFO, "Contact removed :)");
					activity.removeContact(listId);
				}else{
					Toast.makeText(activity, "Contact removing failed :(", Toast.LENGTH_SHORT).show();
					log.log(Level.INFO, "Contact removing failed :(");
				}
			}
		}.execute();
	}
	
	public void messagesBy(final String userId, final MessagesFragment activity) {
		Logger.getLogger("ENDPOINT").log(Level.INFO, "messagesBy");
		
		new AsyncTask<Void, Void, List<ChatMessage>>() {
			protected List<ChatMessage> doInBackground(Void... trash){
				try {
					return messenger.messages().by(userId).execute().getItems();
				} catch (IOException e) {
					e.printStackTrace();
					return null;
				}
			}
			protected void onPostExecute(List<ChatMessage> list){
				log.log(Level.INFO, "onPostExecute messagesBy");
				if(list == null){
					Toast.makeText(activity.getActivity(), "Listing messages failed :(", Toast.LENGTH_SHORT).show();
					log.log(Level.INFO, "Listing messages failed :(");
				}else{
					Toast.makeText(activity.getActivity(), "Listed messages: "+list.size(), Toast.LENGTH_SHORT).show();
					log.log(Level.INFO, "Listed messages: "+list.size());
					activity.setMessages(list);
				}
			}
		}.execute();
	}
	
	public void sendTo(final String to, final String msg, String attachment, final double latitude, final double longitude, final ChatActivity activity) {
		Logger.getLogger("ENDPOINT").log(Level.INFO, "sendTo");
		
		final ChatMessage message = new ChatMessage();
		message.setTo(to);
		message.setMsg(msg);
		message.setLatitude(latitude);
		message.setLongitude(longitude);
		message.setAttachment(attachment);
		//message.encodeAttachment(attachment); // base64
		
		new AsyncTask<Void, Void, Exception>() {
			protected Exception doInBackground(Void... trash){
				try {
					log.log(Level.WARNING, "starting sending...");
					messenger.messages().sendTo(message).execute();
				} catch (IOException e) {
					return e;
				}
				return null;
			}
			protected void onPostExecute(Exception e){
				log.log(Level.WARNING, "onPostExecute sendTo");
				if(e == null){
					Toast.makeText(activity.getApplicationContext(), "Sended :)", Toast.LENGTH_SHORT).show();
					log.log(Level.WARNING, "Sended :)");
					Intent intent2 = new Intent(Common.ACTION_REFRESH);
			        activity.sendBroadcast(intent2);
				}else{
					Toast.makeText(activity.getApplicationContext(), "Sending failed :(", Toast.LENGTH_SHORT).show();
					log.log(Level.WARNING, "Sending failed :(");
					e.printStackTrace();
				}
			}
		}.execute();
	}
	
	public void loadProfilePicture(final String profilePicture, final ImageView image){
    	new AsyncTask<Void, Void, Bitmap>() {
			protected Bitmap doInBackground(Void... trash){
				try{
		        	return bitmapFromUrl(profilePicture);
		        }catch(IOException e){
		        	Toast.makeText(image.getContext().getApplicationContext(), "Photo failed :(", Toast.LENGTH_SHORT).show();
		        	e.printStackTrace();
		        	return null;
		        }
			}
			protected void onPostExecute(Bitmap bitmap){
				final float scale = image.getContext().getResources().getDisplayMetrics().density;
				int pixels = (int) (45.0 * scale + 0.5f);
				image.getLayoutParams().height = pixels;
				image.getLayoutParams().width = pixels;
				image.setScaleType(ScaleType.FIT_XY);
				image.requestLayout();

	            image.setImageBitmap(bitmap);
			}
		}.execute();
    }
	    

    private Bitmap bitmapFromUrl(String url) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        connection.connect();
        InputStream input = connection.getInputStream();
        return BitmapFactory.decodeStream(input);
    }
}

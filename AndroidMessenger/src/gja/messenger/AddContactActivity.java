package gja.messenger;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.appspot.gja_messenger.messenger.model.Profile;

public class AddContactActivity extends ListActivity {

	private ArrayList<Profile> profiles = new ArrayList<Profile>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().getDecorView().setBackgroundColor(Color.WHITE);
		
		
		ProfileAdapter adapter = new ProfileAdapter(this,R.layout.main_list_item, profiles);					
		setListAdapter(adapter);
		
		Endpoint.instance.profilesList(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		Profile profile = profiles.get(position);
		Endpoint.instance.contactsAdd(profile.getName(), profile.getUserId(), MainActivity.mainActivity);
		Intent intent = new Intent(this, ContactsActivity.class);
		startActivity(intent);
		finish();
	}
	
	public void setProfileList(List<Profile> list) {
		profiles.clear();
		profiles.addAll(list);
		((ArrayAdapter<Profile>) getListAdapter()).notifyDataSetChanged();
	}
	
	public static class ProfileAdapter extends ArrayAdapter<Profile> {

	    private static class ViewHolder {
	        private TextView itemView;
	    }

	    public ProfileAdapter(Context context, int textViewResourceId, ArrayList<Profile> items) {
	        super(context, textViewResourceId, items);
	    }

	    public View getView(int position, View convertView, ViewGroup parent) {
    	   View v = convertView;
    	    if(v==null) {
    	    	LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	    	v=vi.inflate(R.layout.main_list_item, null);
    	    }
    	    
			Profile message = getItem(position);  	    
			TextView tv = (TextView)v.findViewById(R.id.text1);
			tv.setText(message.getName());
			tv = (TextView)v.findViewById(R.id.text2);
			tv.setText(message.getEmail());
			
			Endpoint.instance.loadProfilePicture(message.getPicture(), (ImageView)v.findViewById(R.id.avatar));
	    				
			return v;
	    }
	}


}

package gja.messenger;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.text.TextUtils;

import com.appspot.gja_messenger.messenger.model.ChatMessage;
import com.google.android.gms.gcm.GoogleCloudMessaging;

/**
 * @author appsrox.com
 *
 */
public class GcmBroadcastReceiver extends BroadcastReceiver {
	
	private static final String TAG = "GcmBroadcastReceiver";
	
	private Context ctx;	

	@Override
	public void onReceive(Context context, Intent intent) {
		ctx = context;
		
		PowerManager mPowerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
		WakeLock mWakeLock = mPowerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
		mWakeLock.acquire();
		
		try {
			GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
			
			String messageType = gcm.getMessageType(intent);
			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
				sendNotification("Send error", null, null, null, null, false);
				
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
				sendNotification("Deleted messages on server", null, null, null, null, false);
				
			} else {
				String msg = intent.getStringExtra("msg");
				String fromId = intent.getStringExtra("fromId");
				String toId = intent.getStringExtra("toId");
				
				if(msg == null) msg = "";
				if(fromId == null) fromId = "";
				if(toId == null) toId = "";
				
				// ulozeni do lokalni databaze - chceme ponechat?
				ContentValues values = new ContentValues(2);
				values.put(DataProvider.COL_MSG, msg);
				values.put(DataProvider.COL_FROM, fromId);

		    	Intent intent2 = new Intent(Common.ACTION_REFRESH);
		        ctx.sendBroadcast(intent2);
				if (Common.isNotify()) {
					Endpoint.instance.showMessageNotification("New message: "+msg, fromId, toId, this);
				}
			}
			setResultCode(Activity.RESULT_OK);
			
		} finally {
			mWakeLock.release();
		}
	}
	
	public void sendNotification(String text, String fromId, String fromName, String fromPicture, String toId, boolean launchApp) {
		NotificationManager mNotificationManager = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
		
		Notification.Builder mBuilder = new Notification.Builder(ctx)
			.setAutoCancel(true)
			.setSmallIcon(R.drawable.ic_launcher)
			.setContentTitle(ctx.getString(R.string.app_name))
			.setContentText(text);

		if (!TextUtils.isEmpty(Common.getRingtone())) {
			mBuilder.setSound(Uri.parse(Common.getRingtone()));
		}
		
		if (launchApp) {
			Intent intent = new Intent(ctx, ChatActivity.class);
			intent.putExtra(Common.PROFILE_ID, fromId);
			intent.putExtra(Common.PROFILE_NAME, fromName);
			intent.putExtra(Common.PROFILE_OWNER, toId);
			intent.putExtra(Common.PROFILE_PICTURE, fromPicture);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
			PendingIntent pi = PendingIntent.getActivity(ctx, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
			mBuilder.setContentIntent(pi);
		}
		
		mNotificationManager.notify(1, mBuilder.getNotification());
	}
}

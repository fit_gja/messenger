package gja.messenger;

public interface Constants {
	 
    /**
     * Base URL of the Demo Server (such as http://my_host:8080/gcm-demo)
     */
    String SERVER_URL = "http://1-dot-gja-messenger.appspot.com";
 
    /**
     * Google API project id registered to use GCM.
     */
    String SENDER_ID = "862163872842"; // Console - Overview - Project Number
}
package gja.messenger;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.appspot.gja_messenger.messenger.model.ChatMessage;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;

import android.app.Activity;
import android.app.ListFragment;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

/**
 * A fragment representing a list of Items.
 * <p />
 * <p />
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 * 
 * @author appsrox.com
 */
public class MessagesFragment extends ListFragment {

	private static final SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");
	private static final DateFormat[] df = new DateFormat[] {
			DateFormat.getDateInstance(), DateFormat.getTimeInstance() };

	private OnFragmentInteractionListener mListener;
	private MyClassAdapter adapter;
	private Date now;
	private ArrayList<ChatMessage> messages = new ArrayList<ChatMessage>();
	GoogleAccountCredential credential;
	private SharedPreferences settings;
	private String PREF_ACCOUNT_NAME = "ACCOUNT_NAME";
	private Endpoint endpoint;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (OnFragmentInteractionListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFragmentInteractionListener");
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		now = new Date();
		adapter = new MyClassAdapter(this.getActivity(),
				R.layout.chat_list_item, messages);
		setListAdapter(adapter);

		settings = getActivity().getSharedPreferences("GjaMessenger", 0);
		credential = GoogleAccountCredential
				.usingOAuth2(
						getActivity(),
						Collections
								.singleton("https://www.googleapis.com/auth/userinfo.email"));
		credential.setSelectedAccountName(settings.getString(PREF_ACCOUNT_NAME,
				null));

		// log.log(Level.INFO, "selected?");

		if (credential.getSelectedAccountName() != null) {
			endpoint = new Endpoint(credential);
			endpoint.messagesBy(((ChatActivity) getActivity()).getProfileId(),
					this);
		}

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		getListView().setDivider(null);

		Bundle args = new Bundle();
		args.putString(DataProvider.COL_EMAIL, mListener.getProfileEmail());
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	public interface OnFragmentInteractionListener {
		public String getProfileEmail();
	}

	private String getDisplayTime(String datetime) {
		try {
			Date dt = sdf.parse(datetime);
			if (now.getYear() == dt.getYear()
					&& now.getMonth() == dt.getMonth()
					&& now.getDate() == dt.getDate()) {
				return df[1].format(dt);
			}
			return df[0].format(dt);
		} catch (ParseException e) {
			return datetime;
		}
	}

	// ----------------------------------------------------------------------------

	public static class MyClassAdapter extends ArrayAdapter<ChatMessage> {

		private static class ViewHolder {
			private TextView itemView;
		}

		public MyClassAdapter(Context context, int textViewResourceId,
				ArrayList<ChatMessage> items) {
			super(context, textViewResourceId, items);
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;
			if (v == null) {
				LayoutInflater vi = ((Activity) getContext())
						.getLayoutInflater();
				v = vi.inflate(R.layout.chat_list_item, null);
			}
			LinearLayout root = (LinearLayout) v;
			ChatMessage message = getItem(position);
			if (message.getFrom().equals(
					((ChatActivity) getContext()).getProfileOwner())) {
				root.setGravity(Gravity.RIGHT);
				root.setPadding(50, 10, 10, 10);
			} else {
				root.setGravity(Gravity.LEFT);
				root.setPadding(10, 10, 50, 10);
			}
			TextView tv = (TextView) v.findViewById(R.id.text2);
			tv.setText(message.getDate().toString());
			tv = (TextView) v.findViewById(R.id.text1);
			tv.setText(message.getMsg());

			ImageView img = (ImageView) v.findViewById(R.id.imageView1);
			if (message.getAttachment() == null) {
				img.setVisibility(View.INVISIBLE);
			} else {
				String base = message.getAttachment();
				Log.e("ATTACHMENT ",base);
				img.setImageBitmap(Image.decodeBase64(base));
				img.setVisibility(View.VISIBLE);
			}
			return v;
		}
	}

	public void setMessages(List<ChatMessage> list) {
		messages.removeAll(messages);
		messages.addAll(list);
		((ArrayAdapter<ChatMessage>) getListAdapter()).notifyDataSetChanged();
		scrollMyListViewToBottom();
	}

	public void refresh() {
		endpoint.messagesBy(((ChatActivity) getActivity()).getProfileId(), this);
	}

	private void scrollMyListViewToBottom() {
		getListView().post(new Runnable() {
			@Override
			public void run() {
				// Select the last row so it will scroll into view...
				getListView().setSelection(getListAdapter().getCount() - 1);
			}
		});
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {

		ChatMessage message = messages.get(position);
		String uri = String.format(Locale.ENGLISH, "geo:%f,%f",
				message.getLatitude(), message.getLongitude());
		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
		MessagesFragment.this.startActivity(intent);

	};

}

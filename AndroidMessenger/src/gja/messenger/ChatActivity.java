package gja.messenger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;

import android.app.ActionBar;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

public class ChatActivity extends Activity implements
		MessagesFragment.OnFragmentInteractionListener {

	private EditText msgEdit;
	private Button sendBtn;
	private String profileId, profileName, profileOwner, profilePicture;
	private GcmUtil gcmUtil;

	private static final int RESULT_CAMERA = 1;
	private static final int RESULT_GALERY = 2;
	private Uri fileUri = null;
	private BroadcastReceiver registrationStatusReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent != null
					&& Common.ACTION_REGISTER.equals(intent.getAction())) {
				switch (intent.getIntExtra(Common.EXTRA_STATUS, 100)) {
				case Common.STATUS_SUCCESS:
					getActionBar().setSubtitle("online");
					break;

				case Common.STATUS_FAILED:
					getActionBar().setSubtitle("offline");
					break;
				}
			}
		}
	};

	private BroadcastReceiver refreshReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent != null
					&& Common.ACTION_REFRESH.equals(intent.getAction())) {
				((MessagesFragment) getFragmentManager().findFragmentById(
						R.id.msg_list)).refresh();
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chat);

		profileId = getIntent().getStringExtra(Common.PROFILE_ID);
		profileName = getIntent().getStringExtra(Common.PROFILE_NAME);
		profileOwner = getIntent().getStringExtra(Common.PROFILE_OWNER);
		profilePicture = getIntent().getStringExtra(Common.PROFILE_PICTURE);
		msgEdit = (EditText) findViewById(R.id.msg_edit);
		sendBtn = (Button) findViewById(R.id.send_btn);

		sendBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				send(msgEdit.getText().toString());
				msgEdit.setText(null);
			}
		});

		ActionBar actionBar = getActionBar();
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle(profileName);
		actionBar.setSubtitle("connecting...");
		loadProfilePicture(profilePicture);

		registerReceiver(registrationStatusReceiver, new IntentFilter(
				Common.ACTION_REGISTER));
		registerReceiver(refreshReceiver, new IntentFilter(
				Common.ACTION_REFRESH));
		gcmUtil = new GcmUtil(getApplicationContext());

		// CHAT GALERY, CAMERA BUTTONS
		ImageButton camera = (ImageButton) findViewById(R.id.camera);
		ImageButton galery = (ImageButton) findViewById(R.id.galery1);

		galery.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent galleryIntent = new Intent(
						Intent.ACTION_PICK,
						android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				startActivityForResult(galleryIntent,
						ChatActivity.RESULT_GALERY);
			}
		});
		camera.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				File file = getOutputPhotoFile();
				fileUri = Uri.fromFile(getOutputPhotoFile());
				i.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
				startActivityForResult(i, RESULT_CAMERA);
			}
		});
	}

	private void loadProfilePicture(final String profilePicture) {
		final ChatActivity activity = this;
		new AsyncTask<Void, Void, Drawable>() {
			protected Drawable doInBackground(Void... trash) {
				try {
					return drawableFromUrl(profilePicture);
				} catch (IOException e) {
					e.printStackTrace();
					return null;
				}
			}

			protected void onPostExecute(Drawable drawable) {
				ImageView homeImage = (ImageView) findViewById(android.R.id.home);
				homeImage.setImageDrawable(drawable);
			}
		}.execute();
	}

	private Drawable drawableFromUrl(String url) throws IOException {
		Bitmap bitmap;
		HttpURLConnection connection = (HttpURLConnection) new URL(url)
				.openConnection();
		connection.connect();
		InputStream input = connection.getInputStream();
		bitmap = BitmapFactory.decodeStream(input);
		return new BitmapDrawable(bitmap);
	}

	@Override
	public String getProfileEmail() {
		return profileId;
	}

	@Override
	protected void onPause() {
		// reset new messages count
		ContentValues values = new ContentValues(1);
		values.put(DataProvider.COL_COUNT, 0);
		getContentResolver().update(
				Uri.withAppendedPath(DataProvider.CONTENT_URI_PROFILE,
						profileId), values, null, null);
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		unregisterReceiver(registrationStatusReceiver);
		gcmUtil.cleanup();
		super.onDestroy();
	}

	private void send(final String txt) {
		if (Endpoint.instance != null) {
			Location location = getLastKnownLocation();
			double longitude = location.getLongitude();
			double latitude = location.getLatitude();

			Endpoint.instance.sendTo(profileId, txt, null, latitude, longitude,
					this); // TODO attachment

		} else {
			Toast.makeText(getApplicationContext(), "Endpoint not prepared :(",
					Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		Bitmap bitmap = null;
		if ((requestCode == RESULT_GALERY)
				&& resultCode == Activity.RESULT_OK) {
			if(data.getData()==null){
				Toast.makeText(this, "No image selected!", Toast.LENGTH_SHORT).show();
				return;
			}
			
			try {
				InputStream stream = getContentResolver().openInputStream(data.getData());
				bitmap = BitmapFactory.decodeStream(stream);
				// bitmap = Bitmap.createScaledBitmap(bitmap, 90, 90, true);
				stream.close();
				bitmap = Bitmap.createScaledBitmap(bitmap, 100, 100, false);
				String imageBase64 = Image.encodeTobase64(bitmap);
				
				//Log.e("BASE 64", imageBase64.toCharArray().length+"");
				
				Endpoint.instance.sendTo(profileId, "Sent attachment", imageBase64, 0, 0, this);
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		} else if (requestCode == RESULT_CAMERA) {
			if (resultCode == RESULT_OK) {
				Uri photoUri = null;
				if (data == null) {
					// A known bug here! The image should have saved in fileUri
					Toast.makeText(this, "Image saved successfully",
							Toast.LENGTH_LONG).show();
					photoUri = fileUri;
				} else {
					photoUri = data.getData();
					Toast.makeText(this,
							"Image saved successfully in: " + data.getData(),
							Toast.LENGTH_LONG).show();
				}
				// showPhoto(photoUri);
			} else if (resultCode == RESULT_CANCELED) {
				Toast.makeText(this, "Cancelled", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(this, "Callout for image capture failed!",
						Toast.LENGTH_LONG).show();
			}

		}
	}

	public String getProfileId() {
		return getIntent().getStringExtra(Common.PROFILE_ID);
	}

	public String getProfileOwner() {
		return getIntent().getStringExtra(Common.PROFILE_OWNER);
	}

	private File getOutputPhotoFile() {
		File directory = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				getPackageName());
		if (!directory.exists()) {
			if (!directory.mkdirs()) {
				Log.e("STORAGE", "Failed to create storage directory.");
				return null;
			}
		}
		String timeStamp = new SimpleDateFormat("yyyMMdd_HHmmss")
				.format(new java.util.Date());
		return new File(directory.getPath() + File.separator + "IMG_"
				+ timeStamp + ".jpg");
	}

	private Location getLastKnownLocation() {
		LocationManager mLocationManager = (LocationManager) getApplicationContext()
				.getSystemService(LOCATION_SERVICE);
		List<String> providers = mLocationManager.getProviders(true);
		Location bestLocation = null;
		for (String provider : providers) {
			Location l = mLocationManager.getLastKnownLocation(provider);
			if (l == null) {
				continue;
			}
			if (bestLocation == null
					|| l.getAccuracy() < bestLocation.getAccuracy()) {
				// Found best last known location: %s", l);
				bestLocation = l;
			}
		}
		return bestLocation;
	}
}

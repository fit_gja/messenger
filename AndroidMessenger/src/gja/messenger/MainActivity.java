package gja.messenger;

import java.util.Collections;
import java.util.logging.Logger;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;

public class MainActivity extends Activity implements
		LoaderManager.LoaderCallbacks<Cursor>, ConnectionCallbacks,
		OnConnectionFailedListener {
	
	static MainActivity mainActivity;

	private SharedPreferences settings;
	public static GoogleAccountCredential credential;
	private String PREF_ACCOUNT_NAME = "ACCOUNT_NAME";
	static final int REQUEST_ACCOUNT_PICKER = 2;
	private Logger log = Logger.getLogger("MAINACTIVITY");

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
	    getActionBar().hide();

		setContentView(R.layout.activity_main);
		
		mainActivity = this;
		settings = getSharedPreferences("GjaMessenger", 0);
		credential = GoogleAccountCredential.usingOAuth2(this, Collections.singleton("https://www.googleapis.com/auth/userinfo.email"));
	}
	
	public void login_main(View view){
		startActivityForResult(credential.newChooseAccountIntent(),	REQUEST_ACCOUNT_PICKER);
	}
	
	private void onAccountPicked(String accountName){
		saveSelectedAccountName(accountName);
		credential.setSelectedAccountName(accountName);
		Intent intent = new Intent(this, ContactsActivity.class);
		//Zabrani pridani main activity do zásobníku aktivit
	    startActivity(intent);
	    finish();
	}
	
	private void saveSelectedAccountName(String accountName) {
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(PREF_ACCOUNT_NAME, accountName);
		editor.commit();
	}

	protected void onStart() {
		super.onStart();
	}

	protected void onStop() {
		super.onStop();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	// ----------------------------------------------------------------------------

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		CursorLoader loader = new CursorLoader(this,
				DataProvider.CONTENT_URI_PROFILE, new String[] {
						DataProvider.COL_ID, DataProvider.COL_NAME,
						DataProvider.COL_COUNT }, null, null,
				DataProvider.COL_ID + " DESC");
		return loader;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {

	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		Log.d("CONNECTED", "FAILED");
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		Log.d("CONNECTED", "SUCCESS");
	}

	@Override
	public void onConnectionSuspended(int cause) {
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case REQUEST_ACCOUNT_PICKER:
			if (data != null && data.getExtras() != null) {
				onAccountPicked(data.getExtras().getString(AccountManager.KEY_ACCOUNT_NAME));
			}
			break;
		}
	}
}
